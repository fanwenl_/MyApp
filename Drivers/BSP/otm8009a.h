/*
*************************************************************************************************
*文件：bsp.h
*作者：fanwenl_
*版本：V0.0.1
*日期：2016-05-27
*描述：本文件主要是对STM32F469-Dis外设的定义，包括LED、按键、外部SDRAM、外部QSPI Flash、RF ERPOM等。
**************************************************************************************************
*/
#ifndef __OTM8009A_H
#define __OTM8009A_H

#ifdef __cplusplus
extern "C" {
#endif /*cpluspluse*/

#include "bsp.h"

/************************************LCD有效像素定义****************************************/
#define OTM8009A_480X800_WIDTH              ((uint32_t)480) /*竖屏模式*/
#define OTM8009A_480X800_HEIGHT 			((uint32_t)800)

#define OTM8009A_800X400_WIDTH              ((uint32_t)800)/*横屏模式*/
#define OTM8009A_800X400_HEIGHT 			((uint32_t)480)

/************************************LCD时序定义****************************************/
#define OTM8009A_480X800_HFP 	((uint32_t)20)  /*竖屏模式*/
#define OTM8009A_480X800_HBP	((uint32_t)20)
#define OTM8009A_480X800_HSYNC	((uint32_t)2)
#define OTM8009A_480X800_VFP    ((uint32_t)16)
#define OTM8009A_480X800_VBP    ((uint32_t)16)
#define OTM8009A_480X800_VSYNC 	((uint32_t)1)

/*为什么一样呢，应为竖屏或者是横屏只改变的是有效的像素，对于前沿和后沿来说不变*/
#define OTM8009A_800X400_HFP 	((uint32_t)20)   /*横屏模式*/
#define OTM8009A_800X400_HBP	((uint32_t)20)
#define OTM8009A_800X400_HSYNC	((uint32_t)2)
#define OTM8009A_800X400_VFP    ((uint32_t)16)
#define OTM8009A_800X400_VBP    ((uint32_t)16)
#define OTM8009A_800X400_VSYNC 	((uint32_t)1)

#ifdef __cplusplus
}
#endif/*cplusplus*/

#endif/*__OTM8009A_H